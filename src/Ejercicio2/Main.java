package Ejercicio2;
import java.io.*;
public class Main {
	public static void main(String Arg [])throws IOException{
		BufferedReader in = new BufferedReader(new InputStreamReader (System.in));
		int pregunta;					//variable para ejecutar de nuevo programa
		do {
			String[][] datos;
			int limite;
			System.out.print("Ingrese el numero de personas que va a registrar: ");
			limite = Integer.parseInt(in.readLine());													//limite personas
			while (limite<=0) {
				System.out.print("Numero erroneo, ingreselo nuevamente:  ");
				limite = Integer.parseInt(in.readLine());
			}
			Matriz object = new Matriz();														//matriz
			datos = new String[limite][4];
			for (int j =0;j<limite;j++) {													//lleno de matriz
				System.out.print("Ingrese el nombre de la persona: " );
				datos[j][0] = in.readLine();
				System.out.print("Ingrese el apellido de la persona: " );
				datos[j][1] = in.readLine();
				System.out.print("Ingrese el ID de la persona: " );
				datos[j][2] = in.readLine();
				System.out.print("Ingrese el edad de la persona: " );
				datos[j][3] = in.readLine();
			}
			object.setLimite(limite);
			for (int i = 0; i<limite;i++) {
				for (int j=0;j<4;j++) {
					System.out.print(datos[i][j]+" ");
				}
				System.out.println("\n");
			}
			System.out.println("'Desea ejecutar el programa de nuevo?(si=1)");
			pregunta = Integer.parseInt(in.readLine());
		}while(pregunta==1);																//ejecucion del nuevo programa
		System.out.println("Gracias por usar el programa");
	}
}
