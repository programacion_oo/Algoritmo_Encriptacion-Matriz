package Ejercicio1;
import java.io.*;
public class Main {
	public static void main(String Arg[])throws IOException {
		int pregunta;
		do {
			BufferedReader in = new BufferedReader(new InputStreamReader (System.in));
			String palabra;
			Encriptado calc = new Encriptado();
			System.out.print("Ingrese la palabra: ");
			palabra = in.readLine();
			calc.setPalabra(palabra);
			calc.encriptar();
			System.out.println(calc.getEncrip()+"   "+palabra);
			System.out.print("Desea ejecutar el programa de nuevo?(si==1)");
			pregunta = Integer.parseInt(in.readLine());
		} while (pregunta==1);
	}
}

