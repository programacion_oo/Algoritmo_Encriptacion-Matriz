package Ejercicio1;
public class Encriptado {
	private String palabra;		
	private String encrip = "";
	private char[] encriptado;
	public Encriptado() {
	}
	public void encriptar() {
		encriptado = new char[50];      
		encriptado = palabra.toCharArray();
		for (int i = 0; i < encriptado.length; i++) {
			char ascii2;
			int ascii = (int) encriptado[i];
			if (encriptado[i] == 32) {
				ascii2 = (char) ascii;
			} else if (encriptado[i] != 88 & encriptado[i] != 89 & encriptado[i] != 90 & encriptado[i] != 120
					& encriptado[i] != 121 & encriptado[i] != 122) {
				ascii += 3;
			} else {
				ascii -= 23;
			}
			ascii2 = (char) ascii;
			encriptado[i] = ascii2;
			encrip = encrip + "" + encriptado[i];
		}
	}
	//se generan getter y setter de todas las variables
	public String getPalabra() {
		return palabra;
	}
	public void setPalabra(String palabra) {
		this.palabra = palabra;
	}
	public char[] getEncriptado() {
		return encriptado;
	}
	public void setEncriptado(char[] encriptado) {
		this.encriptado = encriptado;
	}
	public String getEncrip() {
		return encrip;
	}
	public void setEncrip(String encrip) {
		this.encrip = encrip;
	}
}
